<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523205427 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE scrape_queue (product_detail_link VARCHAR(255) NOT NULL, ean VARCHAR(255) DEFAULT NULL, scrape_count INT DEFAULT NULL, last_attempt DATETIME DEFAULT NULL, last_successful_attempt DATETIME DEFAULT NULL, PRIMARY KEY(product_detail_link)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artikel (ean VARCHAR(255) NOT NULL, kategorie VARCHAR(255) DEFAULT NULL, serie VARCHAR(255) DEFAULT NULL, farbe VARCHAR(255) DEFAULT NULL, material VARCHAR(255) DEFAULT NULL, stil VARCHAR(255) DEFAULT NULL, modellart VARCHAR(255) DEFAULT NULL, beschreibung LONGTEXT DEFAULT NULL, beschreibung_html LONGTEXT DEFAULT NULL, titel VARCHAR(255) DEFAULT NULL, rating VARCHAR(255) DEFAULT NULL, preis VARCHAR(255) DEFAULT NULL, rating_count INT DEFAULT NULL, platzierung VARCHAR(255) DEFAULT NULL, aufstellmethode VARCHAR(255) DEFAULT NULL, produktbreite VARCHAR(255) DEFAULT NULL, produkthoehe VARCHAR(255) DEFAULT NULL, produktlaenge VARCHAR(255) DEFAULT NULL, werksgarantie VARCHAR(255) DEFAULT NULL, packungsinhalt VARCHAR(255) DEFAULT NULL, produktgewicht VARCHAR(255) DEFAULT NULL, PRIMARY KEY(ean)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE scrape_queue');
        $this->addSql('DROP TABLE artikel');
    }
}
