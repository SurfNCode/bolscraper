<?php

namespace App\Repository;

use App\Entity\ScrapeQueue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ScrapeQueue|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScrapeQueue|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScrapeQueue[]    findAll()
 * @method ScrapeQueue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScrapeQueueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ScrapeQueue::class);
    }

    public function findProductToScrape()
    {
        return $this->createQueryBuilder('s')
                    ->andWhere('s.lastSuccessfulAttempt IS NULL')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getResult()
            ;
    }

    public function findTop500ProductsToScrape()
    {
        return $this->createQueryBuilder('s')
                    ->andWhere('s.lastSuccessfulAttempt IS NULL')
                    ->andWhere('s.htmlDownloaded IS NULL')
                    ->setMaxResults(200)
                    ->getQuery()
                    ->getResult()
            ;
    }
    // /**
    //  * @return ScrapeQueue[] Returns an array of ScrapeQueue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScrapeQueue
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
