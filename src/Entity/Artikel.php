<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtikelRepository")
 */
class Artikel
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kategorie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $farbe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $material;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stil;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ean;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modellart;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $beschreibung;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $beschreibungHtml;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preis;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ratingCount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $platzierung;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $aufstellmethode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $produktbreite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $produkthoehe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $produktlaenge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $werksgarantie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $packungsinhalt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $produktgewicht;

    public function getKategorie(): ?string
    {
        return $this->kategorie;
    }

    public function setKategorie(string $kategorie): self
    {
        $this->kategorie = $kategorie;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFarbe(): ?string
    {
        return $this->farbe;
    }

    public function setFarbe(string $farbe): self
    {
        $this->farbe = $farbe;

        return $this;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setMaterial(string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getStil(): ?string
    {
        return $this->stil;
    }

    public function setStil(string $stil): self
    {
        $this->stil = $stil;

        return $this;
    }

    public function getEan(): ?string
    {
        return $this->ean;
    }

    public function setEan(string $ean): self
    {
        $this->ean = $ean;

        return $this;
    }

    public function getModellart(): ?string
    {
        return $this->modellart;
    }

    public function setModellart(string $modellart): self
    {
        $this->modellart = $modellart;

        return $this;
    }

    public function getBeschreibung(): ?string
    {
        return $this->beschreibung;
    }

    public function setBeschreibung(string $beschreibung): self
    {
        $this->beschreibung = $beschreibung;

        return $this;
    }

    public function getTitel(): ?string
    {
        return $this->titel;
    }

    public function setTitel(string $titel): self
    {
        $this->titel = $titel;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getPreis(): ?string
    {
        return $this->preis;
    }

    public function setPreis(string $preis): self
    {
        $this->preis = $preis;

        return $this;
    }

    public function getRatingCount(): ?int
    {
        return $this->ratingCount;
    }

    public function setRatingCount(int $ratingCount): self
    {
        $this->ratingCount = $ratingCount;

        return $this;
    }

    public function getPlatzierung(): ?string
    {
        return $this->platzierung;
    }

    public function setPlatzierung(string $platzierung): self
    {
        $this->platzierung = $platzierung;

        return $this;
    }

    public function getAufstellmethode(): ?string
    {
        return $this->aufstellmethode;
    }

    public function setAufstellmethode(string $aufstellmethode): self
    {
        $this->aufstellmethode = $aufstellmethode;

        return $this;
    }

    public function getProduktbreite(): ?string
    {
        return $this->produktbreite;
    }

    public function setProduktbreite(string $produktbreite): self
    {
        $this->produktbreite = $produktbreite;

        return $this;
    }

    public function getProdukthoehe(): ?string
    {
        return $this->produkthoehe;
    }

    public function setProdukthoehe(string $produkthoehe): self
    {
        $this->produkthoehe = $produkthoehe;

        return $this;
    }

    public function getProduktlaenge(): ?string
    {
        return $this->produktlaenge;
    }

    public function setProduktlaenge(string $produktlaenge): self
    {
        $this->produktlaenge = $produktlaenge;

        return $this;
    }

    public function getWerksgarantie(): ?string
    {
        return $this->werksgarantie;
    }

    public function setWerksgarantie(string $werksgarantie): self
    {
        $this->werksgarantie = $werksgarantie;

        return $this;
    }

    public function getPackungsinhalt(): ?string
    {
        return $this->packungsinhalt;
    }

    public function setPackungsinhalt(string $packungsinhalt): self
    {
        $this->packungsinhalt = $packungsinhalt;

        return $this;
    }

    public function getProduktgewicht(): ?string
    {
        return $this->produktgewicht;
    }

    public function setProduktgewicht(string $produktgewicht): self
    {
        $this->produktgewicht = $produktgewicht;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBeschreibungHtml()
    {
        return $this->beschreibungHtml;
    }

    /**
     * @param mixed $beschreibungHtml
     */
    public function setBeschreibungHtml($beschreibungHtml): void
    {
        $this->beschreibungHtml = $beschreibungHtml;
    }

    /**
     * @return mixed
     */
    public function getProductLink()
    {
        return $this->productLink;
    }

    /**
     * @param mixed $productLink
     */
    public function setProductLink($productLink): void
    {
        $this->productLink = $productLink;
    }
}
