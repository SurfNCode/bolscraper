<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScrapeQueueRepository")
 */
class ScrapeQueue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ean;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $productDetailLink;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $scrapeCount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastAttempt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSuccessfulAttempt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $htmlDownloaded;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEan(): ?string
    {
        return $this->ean;
    }

    public function setEan(string $ean): self
    {
        $this->ean = $ean;

        return $this;
    }

    public function getProductDetailLink(): ?string
    {
        return $this->productDetailLink;
    }

    public function setProductDetailLink(string $productDetailLink): self
    {
        $this->productDetailLink = $productDetailLink;

        return $this;
    }

    public function getScrapeCount(): ?int
    {
        return $this->scrapeCount;
    }

    public function setScrapeCount(int $scrapeCount): self
    {
        $this->scrapeCount = $scrapeCount;

        return $this;
    }

    public function getLastAttempt(): ?\DateTimeInterface
    {
        return $this->lastAttempt;
    }

    public function setLastAttempt(\DateTimeInterface $lastAttempt): self
    {
        $this->lastAttempt = $lastAttempt;

        return $this;
    }

    public function getLastSuccessfulAttempt(): ?\DateTimeInterface
    {
        return $this->lastSuccessfulAttempt;
    }

    public function setLastSuccessfulAttempt(?\DateTimeInterface $lastSuccessfulAttempt): self
    {
        $this->lastSuccessfulAttempt = $lastSuccessfulAttempt;

        return $this;
    }

    public function incrementScrapeCount()
    {
        $this->scrapeCount = $this->scrapeCount + 1;
    }

    /**
     * @return mixed
     */
    public function getHtmlDownloaded()
    {
        return $this->htmlDownloaded;
    }

    /**
     * @param mixed $htmlDownloaded
     */
    public function setHtmlDownloaded($htmlDownloaded): void
    {
        $this->htmlDownloaded = $htmlDownloaded;
    }

}
