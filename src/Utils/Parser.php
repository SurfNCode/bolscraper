<?php
/**
 * Created by PhpStorm.
 * User: n.dilthey
 * Date: 2018-12-19
 * Time: 15:26
 */

namespace App\Utils;

use App\Entity\Artikel;
use App\Entity\ProductAttributes;
use App\Entity\ScrapeQueue;
use Doctrine\Common\Persistence\ObjectManager;
use PHPHtmlParser\Dom;

class Parser
{
    private $dom;
    private $em;
    private $productLink;

    public function __construct(ObjectManager $em) {
        $this->dom = new Dom;
        $this->em = $em;
    }

    public function getProductAllLinksOnPage($page)
    {
        sleep(rand(1,3));
        $data = $this->dom->loadFromFile('https://www.bol.com/nl/b/wenko/9991973/?page='.$page.'&view=list');
        $fp = fopen($page.'.txt', 'w');
        fwrite($fp, $data);
        fclose($fp);

        $folder          = __DIR__.'/../Html/';
        $utf8_file_data  = utf8_encode($data);
        file_put_contents($folder.$page.'.html' , $utf8_file_data );

        //pages 1 - 38
        $product = $this->dom->find('.product-title');

        foreach($product as $productDetail) {
            $href = $productDetail->getAttribute('href');
            $q = $this->em->getRepository(ScrapeQueue::class)->findOneBy(['productDetailLink' => $href]);
            if(!$q) {
                $q = new ScrapeQueue();
            }
            $q->setProductDetailLink('https://www.bol.com'.$href);
            $this->em->merge($q);
            $this->em->flush();
        }
    }



    public function getProductDetails()
    {
        while($queItem = $this->em->getRepository(ScrapeQueue::class)->findProductToScrape()) {
            $art = new Artikel();
//            $this->dom->loadFromUrl($product->getProductDetailLink());
//            $this->dom->loadFromUrl('https://www.bol.com/nl/p/autohoes-afdekking-volledig-met-zak/9200000094756992/?suggestionType&#x3D;browse');
            $this->dom->loadFromFile(__DIR__.'/../Html/product_'.$queItem[0]->getId().'.html');
            $this->productLink = $queItem[0]->getProductDetailLink();

            $this->getProductTableInformation($art);
            $this->getTitle($art);
            $this->getSerie($art);
            $this->getPrice($art);
            $this->getNumRatings($art);
            $this->getProductDescription($art);

            $q = $this->em->getRepository(ScrapeQueue::class)->findOneBy([
                'productDetailLink' => $this->productLink
            ]);
            $art->setProductLink($this->productLink);

            if(!$q) {
                $q = new ScrapeQueue();
            }

            $q->setLastAttempt(new \Datetime(date("Y-m-d H:i:s")));
            $q->incrementScrapeCount();
            if($art->getEan()) {
                $q->setEan($art->getEan());
                $q->setProductDetailLink($this->productLink);
                $q->setLastSuccessfulAttempt(new \Datetime(date("Y-m-d H:i:s")));
            }
            $this->em->merge($q);
            $this->em->merge($art);
            dump($art);

            $this->em->flush();
        }
    }

    private function getTitle($art)
    {
        $titleNodes = $this->dom->find('title');
        if(count($titleNodes) == 0) {
            $titleAlternative = $this->dom->find('.h-boxedright--xs');
            try {
                $title = $titleAlternative[1]->firstChild()->text();
                $art->setTitel($title);
            } catch(\Exception $e) {
                $art->setTitel('NA');
            }
        }

        try {
            $title = $titleNodes->firstChild()->text();
            $art->setTitel(str_replace('bol.com | ', '', $title));
        } catch(\Exception $e) {
            $art->setTitel('NA');
        }
//        $this->em->merge($art);
    }

    private function getProductTableInformation($art)
    {
        $titleToDbColumnMap = [
            'Categorie&euml;n' => 'kategorie',
            'Kleur' => 'farbe',
            'Stijl' => 'stil',
            'Soort model' => 'modellart',
            'Plaatsing' => 'platzierung',
            'Ophangwijze' => 'aufstellmethode',
            'Materiaal' => 'material',
            'Product breedte' => 'produktbreite',
            'Product hoogte' => 'produkthoehe',
            'Product lengte' => 'produktlaenge',
            'Fabrieksgarantie' => 'werksgarantie',
            'Verpakkingsinhoud' => 'packungsinhalt',
            'EAN' => 'ean',
            'Product gewicht' => 'produktgewicht',
            'Serie' => 'serie',
            'Breedte' => 'produktbreite',
            'Hoogte' => 'produkthoehe',
            'Gewicht' => 'produktgewicht'
        ];

        $titlesHtml = $this->dom->find('.specs__title');
        $valuesHtml = $this->dom->find('.specs__value');
        if ($titlesHtml && count($titlesHtml) > 0) {
            foreach ($titlesHtml as $index => $titleHtml) {
                $title = trim($titleHtml->firstChild()->text());
                $value = trim($valuesHtml[$index]->firstChild()->text());

                if($value == "") {
//                    dump($index);
//                    dump($valuesHtml[$index]->firstChild());
//                    dump(get_class($valuesHtml[$index]->firstChild()));
//                    dump($valuesHtml[$index]);

                    $tagname = $valuesHtml[$index]->firstChild()->nextSibling()->getTag()->name();
//                    dump($tagname);
                    //handle category list
                    if($tagname == 'ul') {
                        //handle cateogry
//                        dump("category");
                        $categories = $valuesHtml[$index]->find('.specs__category');
                        if($categories && count($categories) > 0) {
                            foreach ($categories as $catIdx => $category) {
                                $categoriesArr[$catIdx] = $category->firstChild()->firstChild()->text();
                            }
                            $value = implode(', ', $categoriesArr);
//                            dump("Kategorienstring:");
//                            dump($value);
//                            dump("current index: ".$catIdx);
                        }
                    }

                    if ($tagname == 'a') {
                        $value = $valuesHtml[$index]->firstChild()->nextSibling()->getAttribute('title');
                    }
                }
                //Save all product attributes to db for informational purposes what other info could be parsed


                $attr = $this->em->getRepository(ProductAttributes::class)
                         ->findOneBy(['attribute' => $title]);
                if(!$attr) {
                    $attr = new ProductAttributes();
                }
                $attr->setProductLink($this->productLink);
                $attr->setAttribute($title);
                $attr->setAttributeValue($value);
                $this->em->persist($attr);
                $this->em->flush();

                $data[$title] = $value;
            }
        } else {
            foreach($titleToDbColumnMap as $idx => $item) {
                $data['title'][$item] = 'NA';
            }
        }

        //Translate dutch keys to german
        foreach($data as $key => $datum) {
            if(isset($titleToDbColumnMap[$key])) {
                $data_[$titleToDbColumnMap[$key]] = $datum;
            }
        }

        foreach($data_ as $key => $value) {
            switch ($key) {
                case 'kategorie':
                    $art->setKategorie($value);
                    break;
                case 'farbe':
                    $art->setFarbe($value);
                    break;
                case 'stil':
                    $art->setStil($value);
                    break;
                case 'modellart':
                    $art->setModellart($value);
                    break;
                case 'platzierung':
                    $art->setPlatzierung($value);
                    break;
                case 'aufstellmethode':
                    $art->setAufstellmethode($value);
                    break;
                case 'material':
                    $art->setMaterial($value);
                    break;
                case 'produktbreite':
                    $art->setProduktbreite($value);
                    break;
                case 'produkthoehe':
                    $art->setProdukthoehe($value);
                    break;
                case 'produktlaenge':
                    $art->setProduktlaenge($value);
                    break;
                case 'werksgarantie':
                    $art->setWerksgarantie($value);
                    break;
                case 'packungsinhalt':
                    $art->setPackungsinhalt($value);
                    break;
                case 'ean':
                    $art->setEan($value);
                    break;
                case 'produktgewicht':
                    $art->setProduktgewicht($value);
                    break;
                case 'serie':
                    $art->setSerie($value);
                    break;
            }
        }
//        $this->em->merge($art);
//        $this->em->flush();
    }

    private function getSerie($art)
    {
        $serie = $this->dom->find('.px_pdp_series_click');
        if(count($serie) == 0) {
            $art->setSerie('NA');
        } else {
            $art->setSerie($serie->getAttribute('title'));
        }
//        $this->em->merge($art);
    }

    private function getPrice($art)
    {
        $price = $this->dom->find('.promo-price');
        try {
            $fraction = $price->find('.promo-price__fraction');
            if(count($price) == 0 ||count($fraction) == 0) {
                $art->setPreis('NA');
            } else {
                if($fraction->firstChild()->text() == '-') {
                    $art->setPreis(substr($price->firstChild()->text(), 0, stripos($price->firstChild()->text(), ' ')));
                } else {
                    $art->setPreis(substr($price->firstChild()->text(), 0, stripos($price->firstChild()->text(), ' ')).','.$fraction->firstChild()->text());
                }
            }
        } catch(\Exception $e) {
            $art->setPreis('NA');
        }
//        $this->em->merge($art);
    }

    private function getNumRatings($art)
    {
        $ratings = $this->dom->find('.pdp-header__meta-item span');
        try {
            $totalRatings = $ratings->firstChild()->text();
            $art->setRatingCount($totalRatings);
        } catch(\Exception $e) {
            $art->setRatingCount(0);
        }

        $ratingHtml = $this->dom->find('.rating-stars__value');
        try {
            $rating = $ratingHtml->firstChild()->nextSibling()->firstChild()->nextSibling()->firstChild()->text();
            $art->setRating($rating);
        } catch(\Exception $e) {
            $art->setRating('NA');
        }
//        $this->em->merge($art);
    }

    private function getProductDescription($art)
    {
        $descElems = $this->dom->find('.product-description');
        $descElemsById = $this->dom->find('.js_full_description');
        if($descElems && count($descElems) > 0) {
            $art->setBeschreibungHtml(trim($descElems->innerHtml()));
            $art->setBeschreibung(trim(filter_var($descElems->innerHtml(), FILTER_SANITIZE_STRING)));
        } elseif($descElemsById && count($descElemsById) > 0) {
            $descText = $descElemsById->firstChild()->nextSibling()->nextSibling()->nextSibling()->firstChild()->nextSibling()->nextSibling()->nextSibling();
            $art->setBeschreibungHtml(trim($descText->innerHtml()));
            $art->setBeschreibung(trim(filter_var($descText->innerHtml(), FILTER_SANITIZE_STRING)));
        } else {
            $art->setBeschreibungHtml('NA');
            $art->setBeschreibung('NA');
        }
//        $this->em->merge($art);
    }

    public function saveHtmlForPage($page, $index)
    {
        $data = $this->dom->loadFromUrl($page);
        $folder          = __DIR__.'/../Html/';
        $utf8_file_data  = utf8_encode($data);
        file_put_contents($folder.'product_'.$index.'.html' , $utf8_file_data );
        return;
    }

    public function saveHtmlForPages()
    {
//        for($i=0;$i<50;$i++) {
        /** @var $products \App\Entity\Artikel*/
        $productQueue = $this->em->getRepository(ScrapeQueue::class)
                             ->findTop500ProductsToScrape();
        foreach ($productQueue as $index => $queItem) {
            sleep(rand(1,4));
            $this->saveHtmlForPage($queItem->getProductDetailLink(), $queItem->getId());
            $queItem->setHtmlDownloaded(true);
            $this->em->persist($queItem);
            $this->em->flush();
        }
        return;
    }
}
